# *gruvy*

a KDE color scheme inspired by gruvbox

## How to install

### Color scheme

1. Copy the `.color` file from `colors` to `~/.local/share/color-schemes/`.
2. Activate color scheme in system settings.

### Konsole/Yakuake color scheme

1. Copy both `.colorscheme` files from `konsole` folder to `~/.local/share/konsole/`.
2. Activate color scheme of choice in Konsole/Yakuake application.

### Kate theme

Use `default` application theme and `KDE` syntax theme.

## Themes/settings that work well with gruvy

Do whatever you want. I, personally, prefer to use the following:

- [Lightly](https://github.com/Luwx/Lightly) as Plasma application style. (Give it a try - it's really awesome!)
- [Breeze for GTK](https://invent.kde.org/plasma/breeze-gtk) as GTK2/3 application style.
- [Papirus-Dark](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme) as icon theme.
- Breeze Light (preinstalled) as mouse cursor theme.

## Last remarks

Have fun. :-)
