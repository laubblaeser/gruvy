# How to setup gruvy for matter

To setup grub with [matter.py]() for gruvy, simply download matter, cd into the directory where you've extracted matter and run this command:

./matter.py -i manjaro microsoft-windows folder _ _ _ _ cog-outline memory -ff ~/Theming/fonts/FiraGO_1001/Fonts/FiraGO_TTF_1001/Roman/FiraGO-Regular.ttf -fn FiraGO Regular -fs 32 -bg 282828 -fg EBDBB2 -hl 458588 -ic D65D0E
 
